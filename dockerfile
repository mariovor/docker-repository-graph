FROM python:3.11.3-slim-bullseye
COPY ./requirements.txt /var/www/requirements.txt
RUN pip install -r /var/www/requirements.txt

ENV DOCKER_REGISTRY_PATH /target

COPY load_registry_to_neo4j.py /opt/
CMD ["python", "/opt/load_registry_to_neo4j.py"]

