# Docker registry graph with Neo4j

Scan the contents of a docker registry and add the graph relation between repository, tags, and blobs
to a Neo4j instance. The size (in MB) of the blobs is added as property of the blobs.

Can be used to attribute consumed hard disk space to singe repositories.

To run script `load_registry_to_neo4j.py` following environment variables must be set

- NEO4J_USER: The user to connect to the Neo4j database.
- NEO4J_PASSWORD: The password of the user.
- NEO4J_HOST: The host, including port nummer, e.g. `localhost:7687` (for now, only unencrypted connection supported).
- DOCKER_REGISTRY_PATH: The path to the docker registry, e.g. `/some/path/docker/registry/v2`

## Running on docker
Please check the latest docker tag!

You must mount the folder containing the repository data under `/target` and pass as environment variables
the credentials and host of the Neo4j instance you want to use.
```
docker run --network host  --rm \
-v /some/path/docker/registry/v2:/target \
-e NEO4J_USER=<USER> \
-e NEO4J_PASSWORD=<PASSWORD> \
-e NEO4J_HOST=localhost:7687 \
registry.gitlab.com/mariovor/docker-repository-graph:1.1.0
```




## Cypher Examples

### Sum all blobs related to a repository
Sum all blobs, deduplicated blobs between repositories are re-counted.
```
MATCH(r:Repository)<--(t:Tag)<--(b:Blob)
with r, collect(distinct b) as distinctBlobs
RETURN r.name,  reduce(totalSum = 0, n IN distinctBlobs | totalSum + n.size_in_mb) as size_sum
ORDER BY size_sum DESC
```

### Show largest blobs and related tags
Print in descending order the largest blobs and the tags related to the blob
```
match (blob:Blob)-[relation:TAGGED_BY]->(tag:Tag)
return blob.id, blob.size_in_mb, count(relation), collect(tag.name)
order by blob.size_in_mb DESC
```
