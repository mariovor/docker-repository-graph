import json
import logging
import os
from os.path import isfile, join, isdir
from neo4j import GraphDatabase

USER = os.getenv('NEO4J_USER')
PASSWORD = os.getenv('NEO4J_PASSWORD')
URI = f'neo4j://{os.getenv("NEO4J_HOST")}'

PATH_TO_REGISTRY = os.getenv('DOCKER_REGISTRY_PATH')

PATH_TO_REPOSITORIES = PATH_TO_REGISTRY + '/repositories'
PATH_TO_BLOBS = PATH_TO_REGISTRY + '/blobs'
MANIFESTS_FOLDER = '_manifests'

logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger('default')


def add_repo_and_contents(repo_dir: str, driver):
    """
    Starting from repo_dir, find all repositories (sub-repositories exist) and add
    * repositories,
    * tags,
    * and the relations between repositories and tags
    * and the relations between tags and blobs
    to the database.
    :param repo_dir: The starting folder to search for repositories
    :param driver: A Neo4j driver to communicate with the database
    """
    sub_repos = get_sub_repos(repo_dir)
    for sub_repo in sub_repos:
        add_repo_and_contents(join(repo_dir, sub_repo), driver)

    # This is actually a folder that contains repo data
    if isdir(join(repo_dir, MANIFESTS_FOLDER)):
        repo_name = repo_dir.replace(f"{PATH_TO_REPOSITORIES}/", "")
        LOGGER.info(f'Reading repo {repo_name}')
        write_repo_to_db(driver, repo_name)
        tags, tags_directory = get_tag_names_and_directory_path(repo_dir)
        LOGGER.info(f'Adding {len(tags)} tags from repo {repo_name}')

        # Read tags
        for tag in tags:
            unique_tag_name = f'{repo_name}:{tag}'
            blob_ids_of_tag_metadata, blobs = get_blobs_and_ids_associated_to_tag(tag, tags_directory)
            write_blob_tag_repo_relations_to_db(driver, blobs, blob_ids_of_tag_metadata, repo_name, unique_tag_name)


def get_blobs_and_ids_associated_to_tag(tag: str, tags_directory: str) -> ([str], [str]):
    """
    For a given tag, return the ids of the blobs containing the metadata and the blob ids of the content
    :param tag: Name of the tag
    :param tags_directory: Directory of the tags
    :return: (List of ids of blobs containing the metadata of tag, list of ids of blobs containing the content)
    """
    blob_ids_of_tag_metadata = get_blob_id_of_tag_metadata(tag, tags_directory)
    blobs = []
    for blob_id_of_tag_metadata in blob_ids_of_tag_metadata:
        blobs.extend(get_blobs_from_tag_metadata(blob_id_of_tag_metadata))
    return blob_ids_of_tag_metadata, blobs


def write_repo_to_db(driver, repo_name: str):
    """
    Add the Repository to the database.
    :param driver: A Neo4j driver to communicate with the database
    :param repo_name: The name of the repository
    """
    with driver.session() as session:
        session.execute_write(add_repo_to_db, repo=repo_name)


def get_tag_names_and_directory_path(repo_dir: str) -> ([str], str):
    """
    Given a directory to a repo, extract the names of present tags the path to the folder containing the tags.
    :param repo_dir: A path to a repository
    :return: A tuple consisting of a list of tag names and the path to the folder containing the tags
    """
    tags_directory = join(repo_dir, MANIFESTS_FOLDER, 'tags')
    tags = get_directories(tags_directory)
    return tags, tags_directory


def get_blob_id_of_tag_metadata(tag: str, tags_directory: str) -> [str]:
    """
    Returns the ids of the blobs containing the metadata for the tag.
    :param tag: Name of the tag
    :param tags_directory: Path to the directory containing the tags
    :return: The id of the blob containing the metadata for the tag
    """
    return get_directories(join(tags_directory, tag, 'index', 'sha256'))


def get_blobs_from_tag_metadata(blob_id_of_tag_metadata: str) -> [str]:
    """
    Return the blob ids to which the tag points from the metadata file
    :param blob_id_of_tag_metadata: The id of the blob containing the metadata for the tag
    :return: The blob ids to which the tag points
    """
    blobs = []
    with open(get_blob_path(blob_id_of_tag_metadata)) as tag_meta_file:
        content = json.loads(tag_meta_file.read())
        # Get the digest values, remove the sha256 in front
        for layer in content['layers']:
            blobs.append(layer['digest'][7:])
    return blobs


def write_blob_tag_repo_relations_to_db(driver, blob_ids: [str], blob_ids_of_tag_metadata: [str], repo_name: str,
                                        tag_name: str):
    """
    Write the relations between tag and repo, tag and content blob, and tag and metadata blob id.
    Tags are created if they do not yet exist.
    :param driver: A Neo4j driver to communicate with the database
    :param blob_ids: The blob ids to which the tag points
    :param blob_ids_of_tag_metadata: The id of the blob containing the metadata for the tag
    :param repo_name: Name of the repository
    :param tag_name: Name of the tag
    """
    with driver.session() as session:
        session.execute_write(add_tag_to_db, tag_name)
        session.execute_write(add_tag_repo_relation, repo_name, tag_name)
        for blob_id_of_tag_metadata in blob_ids_of_tag_metadata:
            session.execute_write(add_tag_metadata_blob_relation, tag_name, blob_id_of_tag_metadata)
        for blob_id in blob_ids:
            session.execute_write(add_tag_blob_relation, tag_name, blob_id)


def get_sub_repos(current_dir: str) -> [str]:
    """
    Get list of sub-repos.
    :param current_dir: The path to scan for sub-reps
    :return: A list of sub-repo names
    """
    dirs = get_directories(current_dir)
    return list(filter(lambda x: not x.startswith('_'), dirs))


def main():
    driver = create_driver()

    add_blobs(PATH_TO_BLOBS, driver)
    main_repos = get_directories(PATH_TO_REPOSITORIES)
    for repo in main_repos:
        add_repo_and_contents(join(PATH_TO_REPOSITORIES, repo), driver)


def create_driver():
    """
    Create a driver to communicate with Neo4j.
    :return: A Neo4j driver to communicate with the database
    """
    driver = GraphDatabase.driver(URI, auth=(USER, PASSWORD))
    driver.verify_connectivity()
    return driver


def get_directories(parent_directory: str) -> [str]:
    """
    Get directory names inside a directory.
    :param parent_directory: The path to the directory to get sub-directories.
    :return: A list if directory names.
    """
    return [directory for directory in os.listdir(parent_directory) if
            not isfile(join(parent_directory, directory))]


def get_blob_path(blob_id: str) -> str:
    """
    For a given blob id (as sha256 hash) get the path to the data.
    :param blob_id: Blob_id
    :return: Path to the data file of the blob id.
    """
    return join(PATH_TO_BLOBS, 'sha256', blob_id[:2], blob_id, 'data')


def add_blobs(path_to_blobs: str, driver):
    """
    Add all bobs of the repository to the database.
    :param path_to_blobs: Path to the directory containing the blobs
    :param driver: A Neo4j driver to communicate with the database
    """
    path_to_first_layer_directories = join(path_to_blobs, 'sha256')
    first_layer_directories = os.listdir(path_to_first_layer_directories)
    blobs = []
    for first_layer_dir in first_layer_directories:
        blob_directories = os.listdir(join(path_to_first_layer_directories, first_layer_dir))
        for blob_id in blob_directories:
            blobs.append((blob_id, os.stat(
                join(path_to_first_layer_directories, first_layer_dir, blob_id, 'data')).st_size / (1024 * 1024)))

    LOGGER.info(f'Found total of {len(blobs)} blobs.')
    with driver.session() as session:
        for blob_id, size_in_mb in blobs:
            session.execute_write(add_blob_to_db, blob_id, size_in_mb, )


def add_blob_to_db(tx, blob, size_in_mb):
    tx.run('''
    MERGE (b:Blob {id: $id}) 
    SET b.size_in_mb = $size
    '''
           , id=blob,
           size=size_in_mb)


def add_repo_to_db(tx, repo):
    return tx.run("""
        MERGE (r:Repository {name: $repo})
        """, repo=repo)


def add_tag_to_db(tx, tag):
    return tx.run("""
        MERGE (t:Tag {name: $tag})
        """, tag=tag)


def add_tag_repo_relation(tx, repo, tag):
    return tx.run("""
    MATCH (r:Repository {name: $repo})
    MATCH (t:Tag {name: $tag})
    MERGE (t)-[:TAG_OF]->(r)
    """,
                  repo=repo,
                  tag=tag)


def add_tag_blob_relation(tx, tag, blob):
    return tx.run(
        """
    MATCH (t:Tag {name: $tag})
    MATCH (b:Blob {id: $id})
    MERGE (t)<-[:TAGGED_BY]-(b)    
        """,
        tag=tag,
        id=blob
    )


def add_tag_metadata_blob_relation(tx, tag, blob):
    return tx.run(
        """
    MATCH (t:Tag {name: $tag})
    MATCH (b:Blob {id: $id})
    MERGE (t)<-[:METADATA_OF]-(b)    
        """,
        tag=tag,
        id=blob
    )


if __name__ == '__main__':
    main()
